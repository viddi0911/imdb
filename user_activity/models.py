from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import post_delete, post_save
from movies.models import Movie

# Create your models here.

class UserActivity(models.Model):
    user = models.ForeignKey(User)
    movie = models.ForeignKey(Movie)
    rating = models.IntegerField(default=0,validators = [MaxValueValidator(10),MinValueValidator(0)]) 
    rated_on = models.DateTimeField(auto_now_add = True)
    #class Meta:
    #    unique_together = (('user','movie',),)

def update_movie(instance, **kwargs):
    movie_popularity_point = instance.movie.popularity_points
    rating_count = instance.movie.count

    instance.movie.popularity_points = movie_popularity_point + instance.rating
    instance.movie.count = rating_count + 1 
    instance.movie.imdb_score = float(instance.movie.popularity_points/(rating_count+1))
    instance.movie.save()

post_save.connect(update_movie, sender=UserActivity)
