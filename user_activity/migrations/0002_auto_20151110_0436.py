# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_activity', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='useractivity',
            unique_together=set([('user', 'movie')]),
        ),
    ]
