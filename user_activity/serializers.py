from rest_framework import serializers
from django.contrib.auth.models import User
from user_activity.models import UserActivity

class UserSerializer(serializers.ModelSerializer):

    #created_by = serializers.ReadOnlyField(source='created_by.username')
    #slug = serializers.ReadOnlyField()
    class Meta:
        model = User 
        #fields = ('name', 'slug','description')

class UserActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = UserActivity 
        #fields = ('name', 'slug','description')
