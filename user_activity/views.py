from django.shortcuts import render
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions
from core.permissions import IsAdmin, IsMovieManiac 
from user_activity.serializers import UserSerializer, UserActivitySerializer
from user_activity.models import UserActivity


class UserDetail(generics.RetrieveUpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserSerializer
    queryset = User.objects.all()

class UserActivityList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserActivitySerializer
    queryset = UserActivity.objects.all().order_by('rating')
