from rest_framework import permissions

def user_groups(request):
    return request.user.groups.all().values_list('user__groups__name',flat=True)

class IsAdmin(permissions.BasePermission):

    def has_permission(self, request, view):
        if 'admin' in user_groups(request) or request.user.is_superuser:
            return True

class IsMovieManiac(permissions.BasePermission):

    def has_permission(self, request, view):
        if 'movie_maniac' in user_groups(request):
            return True

