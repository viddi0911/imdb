from django.conf.urls import include, url
from movies.views import MovieList, MovieDetail, GenreList, MovieCreate, MovieRate
from user_activity.views import UserDetail, UserActivityList# UserActivityDetail 

urlpatterns = [

    url(r'^movie-add/$',
       MovieCreate.as_view(),
        name='movies-add'),

    url(r'^movie-list/$',
       MovieList.as_view(),
        name='movies-list'),

    url(r'^movie/(?P<slug>[\w-]+)/$',
        MovieDetail.as_view(),
        name='movie-detail'),

    url(r'^rate-movie/$',
        MovieRate.as_view(),
        name='rate-movie'),

    url(r'^genre/$',
       GenreList.as_view(),
        name='genre-list'),

    url(r'^user/(?P<pk>[0-9]+)/$',
        UserDetail.as_view(),
        name='user-detail'),

    url(r'^user-activity/$',
        UserActivityList.as_view(),
        name='user-activity'),
#
#    url(r'^user-activity/(?<pk>[0-9]+)/$',
#        UserActivityDetail.as_view(),
#        name='user-activity'),
    ]

# Login and logout views for the browsable API
urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]
