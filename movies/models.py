from django.db import models
from django.template.defaultfilters import slugify

class Genre(models.Model):
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=50,null=True, blank=True)

    def save(self,*args,**kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Genre,self).save()

    def __unicode__(self):
        return self.name
   

class Movie(models.Model):
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=50)
    director = models.CharField(max_length=50,null=True,blank=True)
    imdb_score = models.FloatField(default=0.0)
    popularity_points = models.FloatField(default=0.0)
    genre = models.ManyToManyField(Genre,related_name='genres')
    count = models.IntegerField(default=0)

    def save(self,*args,**kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Movie,self).save()

    def __unicode__(self):
        return self.name
