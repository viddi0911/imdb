from rest_framework import serializers
from django.contrib.auth.models import User
from movies.models import Genre, Movie
from user_activity.models import UserActivity

class BaseUser(serializers.ModelSerializer):
    id =serializers.ReadOnlyField()
    class Meta:
        model = User
        fields = ('id','username',)
        read_only_fields = ('id','username',)

class MovieAdminSerializer(serializers.ModelSerializer):
    """ 
    Admin can make movie entry 
    """

    class Meta:
        model = Movie 
        fields = ('name','director','genre','slug','imdb_score','popularity_points')
        read_only_fields = ('slug','imdb_score','popularity_points',)

class MovieManiacSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie 
        fields = ('name','director','genre','slug','imdb_score','popularity_points',)
        read_only_fields = ('name','director','genre','slug','imdb_score',)

class UserActivitySerializer(serializers.ModelSerializer):

    user = BaseUser(required=False,read_only = True)

    class Meta:
        model = UserActivity 
        fields = ('user','movie','rating','rated_on',)
        read_only_fields = ('rated_on',)

class GenreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Genre 
        fields = ('name','slug',)
        read_only_fields = ('slug',)
