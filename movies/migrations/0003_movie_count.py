# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0002_auto_20151108_1808'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='count',
            field=models.IntegerField(default=0),
        ),
    ]
