# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('slug', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('slug', models.CharField(max_length=50)),
                ('director', models.CharField(max_length=50, null=True, blank=True)),
                ('imdb_score', models.FloatField(default=0.0)),
                ('popularity_points', models.FloatField(default=0.0)),
                ('genre', models.ManyToManyField(related_name='genres', to='movies.Genre')),
            ],
        ),
    ]
