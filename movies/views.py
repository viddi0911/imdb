from django.template.defaultfilters import slugify
from rest_framework import generics
from rest_framework import permissions
from core.permissions import IsAdmin, IsMovieManiac
from movies.models import Movie, Genre
from movies.serializers import MovieAdminSerializer, MovieManiacSerializer, GenreSerializer, UserActivitySerializer
from core.permissions import user_groups
from user_activity.models import UserActivity

class MovieCreate(generics.CreateAPIView):

    permission_classes = (IsAdmin,)
    serializer_class = MovieAdminSerializer
    queryset = Movie.objects.all()

class MovieList(generics.ListAPIView):
    queryset = Movie.objects.all()
    def get_serializer_class(self):
        if 'admin' in user_groups(self.request):
            return MovieAdminSerializer
        elif 'movie_maniac' in user_groups(self.request):
            return MovieManiacSerializer

class MovieDetail(generics.RetrieveUpdateAPIView):
    lookup_field = 'slug'
    permission_classes = (IsAdmin,)
    queryset = Movie.objects.all()
    serializer_class = MovieAdminSerializer 

class MovieRate(generics.ListCreateAPIView):
    lookup_field = 'movie__slug'
    queryset = UserActivity.objects.all()
    serializer_class = UserActivitySerializer 
    def perform_create(self, serializer):
        serializer.validated_data['user'] = self.request.user
        serializer.save(user=self.request.user)

class GenreList(generics.ListCreateAPIView):
    permission_classes = (IsAdmin,)
    serializer_class = GenreSerializer
    queryset = Genre.objects.all()

